#!/bin/bash

# Script to update main.pot and *.po files
#
# This Script is released under GPL v3 license
# Copyright (C) 2020-2022 Javad Rahmatzadeh

set -e

# cd to the repo root
cd "$( cd "$( dirname "$0" )" && pwd )/.."

xgettext \
    --add-comments="TRANSLATORS:" \
    --from-code=UTF-8 \
    --copyright-holder="Marcos Alvarez" \
    --package-name="thinkpad-battery-threshold-extension" \
    --package-version="1" \
    --output="po/main.pot" \
    src/ui/*.ui \
    src/*.js \
    src/libs/*.js \
    src/preferences/*.js

for file in po/*.po
do
    echo -n "Updating $(basename "$file" .po)"
    msgmerge --backup=off -U "$file" po/main.pot
  
    if grep --silent "#, fuzzy" "$file"; then
        fuzzy+=("$(basename "$file" .po)")
    fi
done

if [[ -v fuzzy ]]; then
    echo "WARNING: Translations have unclear strings and need an update: ${fuzzy[*]}"
fi