'use strict';

import { ExtensionPreferences } from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

import { General } from './preferences/general.js';
import { Thinkpad } from './preferences/thinkpad.js';
import { About } from './preferences/about.js';

import { ThinkPad } from './libs/driver.js';

export default class ThinkpadPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        window._settings = this.getSettings();
        window._driver = new ThinkPad({'settings': window._settings});
        window._metadata = this.metadata;
        
        window.add(new General(window));
        window.add(new Thinkpad(window));
        window.add(new About(window));
        
        window.search_enabled = true;
    }
}