'use strict'

import GLib from 'gi://GLib';
import Adw from 'gi://Adw';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';

import * as Utils from './utils.js';

export const General = GObject.registerClass({
    GTypeName: 'GeneralPrefs',
    Template: GLib.Uri.resolve_relative(import.meta.url, '../ui/general.ui',GLib.UriFlags.NONE),
    InternalChildren: [
        'indicator_mode',
        'color_mode',
        'show_values',
        'show_notifications',
        'reset_all',
        'reset_dialog',
    ],
}, class General extends Adw.PreferencesPage {
    constructor(window) {
        super({});

        Utils.bindAdwComboRow(this._indicator_mode, window._settings, 'indicator-mode');
        window._settings.bind(
            'color-mode', 
            this._color_mode, 
            'active', 
            Gio.SettingsBindFlags.DEFAULT
        );
        window._settings.bind(
            'show-current-values', 
            this._show_values, 
            'active', 
            Gio.SettingsBindFlags.DEFAULT
        );
        window._settings.bind(
            'show-notifications', 
            this._show_notifications, 
            'active', 
            Gio.SettingsBindFlags.DEFAULT
        );

        this._reset_dialog.connect('response', (obj, response, data) => {
            if (response === 'reset') {
                this._resetSettings(window._settings);
                window._driver.enableAll();
            }
        });

        this._reset_all.connect('clicked', () => {
            this._reset_dialog.transientFor = this.root;
            this._reset_dialog.present();
        });
    }

    /**
     * Reset all (recursively) settings values to default
     * 
     * @param {Gio.Settings} settings Settings to reset
     */
    _resetSettings(settings) {
        const keys = settings.settings_schema.list_keys();
        keys.forEach(key => {
            settings.reset(key);
        });

        const childrens = settings.settings_schema.list_children();
        childrens.forEach(children => {
            const childrenSettings = settings.get_child(children);
            this._resetSettings(childrenSettings);
        });
    }
});