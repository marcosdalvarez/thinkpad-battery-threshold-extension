'use strict'

import GLib from 'gi://GLib';
import Adw from 'gi://Adw';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';
import Gdk from 'gi://Gdk';

// Links
const URLS = {
    readme: 'https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/',
    bug_report: 'https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/issues',
    license: 'https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/blob/main/LICENSE',
    translate: 'https://hosted.weblate.org/engage/thinkpad-battery-threshold/',
};

export const About = GObject.registerClass({
    GTypeName: 'AboutPrefs',
    Template: GLib.Uri.resolve_relative(import.meta.url, '../ui/about.ui',GLib.UriFlags.NONE),
    InternalChildren: [
        'app_icon_image',
        'app_name_label',
        'version_label',
        'open_readme',
        'open_bug_report',
        'open_license',
        'open_translate',
    ]
}, class About extends Adw.PreferencesPage {
    constructor(window) {
        super({});

        this._app_icon_image.gicon = Gio.icon_new_for_string(`${GLib.Uri.resolve_relative(import.meta.url, '../icons/threshold-active.svg',GLib.UriFlags.NONE)}`);
        this._app_name_label.label = window._metadata.name;
        this._version_label.label = window._metadata.version ? window._metadata.version.toString() : "0";

        // Setup actions
        const actionGroup = new Gio.SimpleActionGroup();
        window.insert_action_group('prefs', actionGroup);

        // A list of actions with their associated link
        const actions = [
            {
                name: 'open-bug-report',
                link: URLS.bug_report
            },
            {
                name: 'open-readme',
                link: URLS.readme
            },
            {
                name: 'open-license',
                link: URLS.license
            },
            {
                name: 'open-translate',
                link: URLS.translate
            },
        ];

        actions.forEach(action => {
            let act = new Gio.SimpleAction({ name: action.name });
            act.connect(
                'activate',
                _ => Gtk.show_uri(window, action.link, Gdk.CURRENT_TIME)
            );
            actionGroup.add_action(act);
        });

        // Set tooltips
        this._open_readme.tooltip_text = URLS.readme;
        this._open_license.tooltip_text = URLS.license;
        this._open_bug_report.tooltip_text = URLS.bug_report;
        this._open_translate.tooltip_text = URLS.translate;
    }
});